﻿using UnityEngine;
using System.Collections;

public class ExplosionRadius : MonoBehaviour {

	void Start () {
		StartCoroutine (Expand ());
	}

	IEnumerator Expand () {
		for (float t = 0.0f; t < 1.0f; t += 0.025f) {
			transform.localScale += new Vector3 (5, 5, 5);
			yield return new WaitForSeconds (0.001f);
		}
		Destroy (this.gameObject);
	}
}
