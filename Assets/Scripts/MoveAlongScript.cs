﻿using UnityEngine;
using System.Collections;

public class MoveAlongScript : MonoBehaviour {

	Rigidbody rgd;
	public int pathNumber;

	void Awake () {
		rgd = gameObject.GetComponent<Rigidbody> ();
	}

	void Start () {
		if (pathNumber == 0) {
			StartCoroutine (MoveAlongPath01 ());
		} else if (pathNumber == 1) {
			StartCoroutine (MoveAlongPath02 ());
		} else if (pathNumber == 2) {
			StartCoroutine (MoveAlongPath03 ());
		}
	}

	IEnumerator MoveAlongPath01 () {
		while (true) {
			rgd.AddForce (new Vector3 (0, 3, 0), ForceMode.Impulse); // 0, 3, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (3, -1, 0), ForceMode.Impulse); // 3, 2, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (0, -4, 0), ForceMode.Impulse); // 3, -2, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (-3, -1, 0), ForceMode.Impulse); // 0, -3, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (-3, 1, 0), ForceMode.Impulse); // -3, -2, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (0, 4, 0), ForceMode.Impulse); // -3, 2, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (3, -2, 0), ForceMode.Impulse); // 0, 0, 0
		}
	}

	IEnumerator MoveAlongPath02 () {
		while (true) {
			rgd.AddForce (new Vector3 (0, 5, 0), ForceMode.Impulse); // 0, 5, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (2.5f, -7.5f, 0), ForceMode.Impulse); // 2.5f, -2.5f, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (0, 5, 0), ForceMode.Impulse); // 2.5f, 2.5f, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (-2.5f, -7.5f, 0), ForceMode.Impulse); // 0, -5, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (-2.5f, 7.5f, 0), ForceMode.Impulse); // -2.5f, 2.5f, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (0, -5, 0), ForceMode.Impulse); // -2.5f, -2.5f, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (2.5f, 2.5f, 0), ForceMode.Impulse); // 0, 0, 0
		}
	}

	IEnumerator MoveAlongPath03 () {
		while (true) {
			rgd.AddForce (new Vector3 (2, 7, 0), ForceMode.Impulse); // 2, 7, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (0, -14, 0), ForceMode.Impulse); // 2, -7, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (-7, 11, 0), ForceMode.Impulse); // -5, 4, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (11, -4, 0), ForceMode.Impulse); // 6, 0, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (-11, -4, 0), ForceMode.Impulse); // -5, -4, 0
			yield return new WaitForSeconds (1);
			rgd.AddForce (new Vector3 (5, 4, 0), ForceMode.Impulse); // 0, 0, 0
		}
	}
}
