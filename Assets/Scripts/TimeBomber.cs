﻿using UnityEngine;
using System.Collections;

public class TimeBomber : MonoBehaviour {

	Material mtr;
	Rigidbody rgd;
	Renderer rend;

	public GameObject ExplosionRadiusGameObject;

	void Awake () {
		rend = gameObject.GetComponent<Renderer> ();
		rgd = gameObject.GetComponent<Rigidbody> ();
		mtr = rend.material;
	}

	void Start () {
		StartCoroutine (Blink ());
		StartCoroutine (Boom ());
	}

	IEnumerator Blink () {
		while (true) {
			for (float a = 1.0f; a > 0; a -= 0.1f) {
				Color color = mtr.color;
				color.a = a;
				mtr.color = color;
				yield return new WaitForSeconds (0.05f);
			}
			for (float a = 0; a < 1.0f; a += 0.1f) {
				Color color = mtr.color;
				color.a = a;
				mtr.color = color;
				yield return new WaitForSeconds (0.05f);
			}
		}
	}

	IEnumerator Boom () {
		yield return new WaitForSeconds (10);
		for (int t = 0; t < 5; t++) {
			float x = Random.value * 3f * Random.Range (-1, 2);
			float y = Random.value * 3f;
			float z = Random.value * 3f * Random.Range (-1, 2);
			rgd.AddForce (new Vector3 (x, y, z), ForceMode.Impulse);
			yield return new WaitForSeconds (1);
		}
		Instantiate (ExplosionRadiusGameObject, this.transform.position, Quaternion.identity);
		Destroy (this.gameObject);
	}
}
